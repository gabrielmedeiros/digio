package digital.gok.digioteste

import android.app.Application
import digital.gok.digioteste.di.networkModule
import digital.gok.digioteste.di.productsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DigioApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@DigioApplication)
            modules(listOf(
                networkModule,
                productsModule
            ))
        }
    }
}
