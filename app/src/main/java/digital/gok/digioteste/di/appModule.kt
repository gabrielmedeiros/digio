package digital.gok.digioteste.di

import digital.gok.digioteste.features.main.MainViewModel
import digital.gok.digioteste.repository.ProductsRepository
import digital.gok.digioteste.service.ProductsService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideOkHttpClient() }
    single { provideRetrofit(okHttpClient = get()) }
}

val productsModule = module {
    factory { provideProductsService(retrofit = get()) }
    factory { ProductsRepository(productsService = get()) }
    viewModel { MainViewModel(productsRepository = get()) }
}

fun provideProductsService(retrofit: Retrofit): ProductsService = retrofit.create(ProductsService::class.java)

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient()
}