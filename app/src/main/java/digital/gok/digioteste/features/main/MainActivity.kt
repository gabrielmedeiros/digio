package digital.gok.digioteste.features.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import coil.load
import coil.transform.RoundedCornersTransformation
import digital.gok.digioteste.R
import digital.gok.digioteste.model.products.Cash
import digital.gok.digioteste.model.products.Product
import digital.gok.digioteste.model.products.Spotlight
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupObservers()

        viewModel.getProducts()
    }

    private fun setupObservers() {
        viewModel.products.observe(this, Observer {
            it.data?.spotlight?.let { items ->
                setupSpotlight(items)
            }
            it.data?.cash?.let { cash ->
                setupCash(cash)
            }
            it.data?.products?.let { items ->
                setupProducts(items)
            }
        })
    }

    private fun setupSpotlight(items: List<Spotlight>) {
        val spotlightAdapter = SpotlightAdapter(items)
        viewpagerSpotlight.adapter = spotlightAdapter
    }

    private fun setupCash(cash: Cash) {
        imageCash.load(cash.bannerURL) {
            transformations(RoundedCornersTransformation(15f))
        }
    }

    private fun setupProducts(items: List<Product>) {
        val productAdapter = ProductAdapter(items)
        viewpagerProducts.adapter = productAdapter
        viewpagerProducts.clipToPadding = false
        viewpagerProducts.clipChildren = false
        viewpagerProducts.offscreenPageLimit = items.size

        val pageMarginPx = resources.getDimensionPixelOffset(R.dimen.pageMargin)
        val offsetPx = resources.getDimensionPixelOffset(R.dimen.offset)
        viewpagerProducts.setPageTransformer { page, position ->
            val viewPager = page.parent.parent as ViewPager2
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (viewPager.orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
        }
    }
}
