package digital.gok.digioteste.features.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import digital.gok.digioteste.base.Resource
import digital.gok.digioteste.model.products.Products
import digital.gok.digioteste.repository.ProductsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(
    private val productsRepository: ProductsRepository
) : ViewModel() {

    private val _products = MutableLiveData<Resource<Products>>()
    val products: LiveData<Resource<Products>>
        get() = _products

    fun getProducts() {
        _products.value = Resource.Loading()

        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                productsRepository.get()
            }
            _products.value = Resource.Success(result)
        }
    }
}