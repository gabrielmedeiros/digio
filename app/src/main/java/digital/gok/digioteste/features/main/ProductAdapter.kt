package digital.gok.digioteste.features.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.BlurTransformation
import coil.transform.GrayscaleTransformation
import coil.transform.RoundedCornersTransformation
import digital.gok.digioteste.R
import digital.gok.digioteste.model.products.Product

class ProductAdapter(
    private val items: List<Product>
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val item = items[position]
        holder.setImageView(item.imageURL)
    }

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var imageProduct: AppCompatImageView = itemView.findViewById(R.id.imageProduct)

        fun setImageView(url: String) {
            imageProduct.load(url) {
                transformations(RoundedCornersTransformation(15f))
                transformations(BlurTransformation(itemView.context, 5f))
            }
        }
    }
}