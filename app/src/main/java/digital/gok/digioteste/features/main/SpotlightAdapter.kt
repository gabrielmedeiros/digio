package digital.gok.digioteste.features.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import digital.gok.digioteste.R
import digital.gok.digioteste.model.products.Spotlight

class SpotlightAdapter(
    private val items: List<Spotlight>
) : RecyclerView.Adapter<SpotlightAdapter.SpotlightViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotlightViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_item_spotlight, parent, false)
        return SpotlightViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SpotlightViewHolder, position: Int) {
        val item = items[position]
        holder.setImageView(item.bannerURL)
    }

    class SpotlightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var imageSpotlight: AppCompatImageView = itemView.findViewById(R.id.imageSpotlight)

        fun setImageView(url: String) {
            imageSpotlight.load(url) {
                transformations(RoundedCornersTransformation(15f))
            }
        }
    }
}