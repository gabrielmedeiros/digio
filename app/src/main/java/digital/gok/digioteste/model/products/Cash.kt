package digital.gok.digioteste.model.products

data class Cash(
    val bannerURL: String,
    val description: String,
    val title: String
)