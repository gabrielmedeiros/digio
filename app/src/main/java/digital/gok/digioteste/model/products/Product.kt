package digital.gok.digioteste.model.products

data class Product(
    val description: String,
    val imageURL: String,
    val name: String
)