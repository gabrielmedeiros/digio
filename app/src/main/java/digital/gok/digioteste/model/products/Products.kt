package digital.gok.digioteste.model.products

data class Products(
    val cash: Cash,
    val products: List<Product>,
    val spotlight: List<Spotlight>
)