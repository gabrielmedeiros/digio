package digital.gok.digioteste.model.products

data class Spotlight(
    val bannerURL: String,
    val description: String,
    val name: String
)