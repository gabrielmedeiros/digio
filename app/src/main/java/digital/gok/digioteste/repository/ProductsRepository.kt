package digital.gok.digioteste.repository

import digital.gok.digioteste.model.products.Products
import digital.gok.digioteste.service.ProductsService

class ProductsRepository(
    private val productsService: ProductsService
) {
    suspend fun get(): Products {
        return productsService.get()
    }
}