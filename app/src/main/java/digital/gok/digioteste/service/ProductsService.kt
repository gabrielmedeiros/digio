package digital.gok.digioteste.service

import digital.gok.digioteste.model.products.Products
import retrofit2.http.GET

interface ProductsService {

    @GET("products")
    suspend fun get(): Products
}